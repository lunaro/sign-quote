def price_quote(dim, num_cols)  
  cost = apply_tax(dim_cost(dim), col_cost(num_cols))
  to_cost(cost)
end

def dim_cost(dim)
  15 * dim
end

def col_cost(num_cols)
  (num_cols > 2 ? 15 : 10) * num_cols
end

def apply_tax(dim_cost, col_cost)
  1.15 * (dim_cost + col_cost)
end

def to_cost(cost)
  "$#{cost.round(2)}"
end

puts price_quote(200, 3)